<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

// $context         = Timber::context();
// $timber_post     = Timber::get_post();
// $context['post'] = $timber_post;

// if ( post_password_required( $timber_post->ID ) ) {
// 	Timber::render( 'single-password.twig', $context );
// } else {
// 	Timber::render( array( 'single-' . $timber_post->ID . '.twig', 'single-' . $timber_post->post_type . '.twig', 'single-' . $timber_post->slug . '.twig', 'single.twig' ), $context );
// }


$context = Timber::context();
$context['post'] = Timber::get_post();
$context['posts'] = Timber::get_posts([
    'posts_per_page' => 4,
    'post__not_in' => [$context['post']->ID], // Wyklucz bieżący post
    'category__in' => wp_get_post_categories($context['post']->ID), // Pobierz kategorie bieżącego posta
]);
$context['is_single'] = is_single();

Timber::render('single.twig', $context);
?>