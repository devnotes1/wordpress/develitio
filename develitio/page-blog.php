<?php
/*
Template Name: Blog
*/
$context = Timber::context();
$context['posts'] = Timber::get_posts( [
    'post_type'     => 'post',
] );
Timber::render('page-blog.twig', $context);

?>

