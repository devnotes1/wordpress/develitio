<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 */

// Load Composer dependencies.
require_once __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/src/StarterSite.php';

Timber\Timber::init();

//funkcja do bobierania styli w edytorze wordpressa
//
// function my_acf_block_editor_style()
// {
//     wp_enqueue_style(
//         'example_block_css',
//         get_template_directory_uri() . '/src/css/main.css'
//     );
// }

// add_action('enqueue_block_assets', 'my_acf_block_editor_style');

// Sets the directories (inside your theme) to find .twig files.
Timber::$dirname = [ 'templates', 'views' ];

new StarterSite();
