import * as bootstrap from 'bootstrap';
import Splide from '@splidejs/splide';

//Sprawdza url obecjej strony
// const currentUrl = window.location.href;
// console.log(currentUrl)

const width = window.innerWidth;
const inputs = document.querySelectorAll('.form__input input');
const menuItem = document.querySelectorAll('.my-nav-link');

menuItem.forEach(el => {
	el.addEventListener('click', function (e) {
		// e.preventDefault();
		const dropdown = this.nextElementSibling;
		if (dropdown) {
			if (width < 1200) {
				dropdown.classList.toggle('my-nav__drop--open');
			}
		}
	});
});

const removeMenu = () => {
	const nav = document.querySelector('.my-navbar--home');
	const navLinks = document.querySelectorAll('.my-nav-link');
	const scrolled = window.scrollY;
	const logoWhite = document.querySelector('.logo--white');
	const logoBlack = document.querySelector('.logo--black');
	const en = document.querySelector('.my-en');
	if (width > 1200) {
		if (nav) {
			if (scrolled > 50) {
				console.log('scroll');
				nav.style.backgroundColor = 'white';
				logoWhite.style.display = 'none';
				logoBlack.style.display = 'block';
				navLinks.forEach(element => {
					element.style.color = 'black';
				});
				en.style.color = 'black';
			} else {
				nav.style.backgroundColor = 'transparent';
				logoWhite.style.display = 'block';
				logoBlack.style.display = 'none';
				navLinks.forEach(element => {
					element.style.color = 'white';
				});
				en.style.color = 'white';
			}
		}
	}
};

window.addEventListener('scroll', removeMenu);

var splide = new Splide('.splide', {
	type: 'loop',
	classes: {
		arrows: 'splide__arrows my-splide__arrows',
		arrow: 'splide__arrow my-slide__arrow',
		prev: 'splide__arrow--prev my-slide__arrow-prev',
		next: 'splide__arrow--next my-slide__arrow-next',
	},
	pagination: false,
	mediaQuery: 'min',
	focus: 'center',
	breakpoints: {
		576: {
			fixedWidth: '31rem',
			gap: '2rem',
			padding: '10%',
		},
		768: {
			gap: '1rem',
			fixedWidth: '21rem',
		},
		992: {
			fixedWidth: '19rem',
		},
		1200: {
			fixedWidth: '22rem',
		},
		1500: {
			gap: '2rem',
			fixedWidth: '27rem',
		},
	},
});

splide.mount();
AOS.init();
